﻿using System.Collections.Generic;
using System.Net.Http.Formatting;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;

namespace Yoyocms.Umbraco7.TagManager
{
    [PluginController("TagManager")]
    [Tree("TagManager", "TagManagerTree", "Tag Manager", iconClosed: "icon-doc")]    
    public class TagManagerTreeController : TreeController
    {
        private TagManagerAPIController tmapictlr;

        public TagManagerTreeController()
        {
            tmapictlr = new TagManagerAPIController();
        }

        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            if (id == global::Umbraco.Core.Constants.System.Root.ToString())
            {
                //top level nodes - generate list of tag groups that this user has access to.       
                var tree = new TreeNodeCollection();
                foreach (var tagGroup in tmapictlr.GetTagGroups())
                {
                    var item = CreateTreeNode("tagGroup-" + tagGroup.group, id, null, tagGroup.group, "icon-bulleted-list", true);
                    tree.Add(item);
                }

                return tree;
            }
            else
            {
                //List all tags under group

                //Get tag groupname
                string groupName = id.Substring(id.IndexOf('-') + 1);

                var tree = new TreeNodeCollection();

                IEnumerable<Yoyocms.Umbraco7.TagManager.Models.cmsTags> cmsTags = tmapictlr.GetAllTagsInGroup(groupName);

                foreach (var tag in cmsTags)
                {
                    var item = CreateTreeNode(tag.id.ToString(), groupName, queryStrings, string.Format("{0} ({1})", tag.tag, tag.noTaggedNodes.ToString()), "icon-bulleted-list", false);
                    tree.Add(item);
                }

                return tree;
            }
        }

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            var menu = new MenuItemCollection();

            if (id.Contains("tag-"))
            {
                menu.Items.Add(new MenuItem("delete", "Delete"));
            }

            return menu;
        }
    }
}